package pageFactory.trello;

import core.browsers.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class LoginPage extends BasePage {

    @FindBy(id = "login-form")
    private WebElement formLogIn;

    @FindBy(id = "user")
    private WebElement inputEmail;

    @FindBy(id = "password")
    private WebElement inputPassword;

    @FindBy(id = "login")
    private WebElement btnSubmit;

    public LoginPage(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {
        waitForElementDisplayed(formLogIn, 4);
    }

    @Step("Login with email : {0} and password: {1}")
    public void login(String email, String password){
        waitForElementDisplayed(inputEmail, 4);
        click(inputEmail);
        type(email);
        waitForElementDisplayed(inputPassword, 4);
        click(inputPassword);
        type(password);
        waitForElementDisplayed(btnSubmit, 4);
        click(btnSubmit);
    }

}
