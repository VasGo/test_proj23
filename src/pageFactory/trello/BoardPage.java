package pageFactory.trello;

import core.browsers.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import ru.yandex.qatools.allure.annotations.Step;
import utils.tools.Log;

import java.util.ArrayList;
import java.util.List;

public class BoardPage extends BasePage {

    @FindBy(xpath = "//*[@class='board-header-btn board-header-btn-name js-rename-board']/span")
    private WebElement boardName;

    @FindBy(xpath = "//*[@class='board-header u-clearfix js-board-header']")
    private WebElement createdBoardPage;

    @FindBy(xpath = "//*[@class='list-header-name-assist js-list-name-assist']")
    private WebElement createdList;

    @FindBy(xpath = "//*[@class='primary mod-list-add-button js-save-edit']")
    private WebElement btnAddList;

    @FindBy(xpath = "//*[@class='list-name-input']")
    private WebElement nameList;

    @FindBy(xpath = "//*[@class='list-header-name-assist js-list-name-assist']")
    private List<WebElement> list;

    @FindBy(xpath = "//*[@class='js-add-a-card']")
    private List<WebElement> btnAddCard;

    @FindBy(xpath = "//*[@class='list-card-composer-textarea js-card-title']")
    private WebElement inputCardName;

    @FindBy(xpath = "//*[@class='primary confirm mod-compact js-add-card']")
    private WebElement btnGreenAddCard;

    public BoardPage(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {
        waitForElementDisplayed(createdBoardPage, 4);
    }

    @Step("Add list: {0}")
    public BoardPage addList(String[] list) {
        for (String listElement : list) {
            waitForElementDisplayed(nameList, 4);
            click(nameList);
            type(listElement);
            waitForElementDisplayed(btnAddList, 4);
            click(btnAddList);
            Log.info("Add list: " + listElement);
        }

        return this;
    }

    public String getBoardName() {
        return boardName.getText();
    }

    public String[] getList() {
        List<String> names = new ArrayList<>();
        for (WebElement name : list) {
            names.add(name.getText());
        }
        String[] arrayNames = (String[]) names.toArray();

        return arrayNames;
    }

    @Step("Add card: {0} to list: {1}")
    public void addCard(String name, int listNumber) {
        waitForElementDisplayed(btnAddCard, 4);
        click(btnAddCard.get(listNumber - 1));
        waitForElementDisplayed(inputCardName, 4);
        click(inputCardName);
        type(name);
        waitForElementDisplayed(btnAddCard, 4);
        click(btnGreenAddCard);
        Log.info("Add card: " + name + " to list: " + listNumber);
    }

    public String getCard(int listNumber, int cardNumber) {
        List<String> names = new ArrayList<>();
        By cards = By.xpath("(//*[@class='list-cards u-fancy-scrollbar u-clearfix js-list-cards js-sortable ui-sortable'])[" + listNumber + "]//*[@class='list-card-title js-card-name']");
        List<WebElement> elementsCard = driver.findElements(cards);
        waitForElementDisplayed(elementsCard, 8);
        for (WebElement name : elementsCard) {
            names.add(name.getText());
        }

        return names.get(cardNumber - 1);
    }

}