package pageFactory.trello;

import core.browsers.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class BoardWidget extends BasePage {

    @FindBy(xpath = "//*[@class='board-tile create-board-tile has-photo-background']")
    private WebElement widgetBoard;

    @FindBy(xpath = "//*[@class='subtle-input']")
    private WebElement titleBoard;

    @FindBy(xpath = "//*[@type='submit']")
    private WebElement btnSubmit;

    public BoardWidget(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {
        waitForElementDisplayed(widgetBoard, 4);
    }

    @Step("Input board title: {0}")
    public BoardPage inputBoardName(String name) {
        waitForElementDisplayed(titleBoard, 4);
        click(titleBoard);
        type(name);
        waitForElementDisplayed(btnSubmit, 4);
        click(btnSubmit);
        BoardPage boardPage = new BoardPage(browser);
        boardPage.isOpened();

        return boardPage;
    }


}
