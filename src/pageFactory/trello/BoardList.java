package pageFactory.trello;

import core.browsers.Browser;

import java.util.List;

public class BoardList extends BoardPage {

    public BoardList(Browser browser) {
        super(browser);
    }

    protected List<Card> cards;
}
