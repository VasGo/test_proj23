package pageFactory.trello;

import core.browsers.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import ru.yandex.qatools.allure.annotations.Step;

public class BoardsPage extends BasePage {

    @FindBy(xpath = "//*[@class='boards-page-board-section-header-name']")
    private WebElement pageBoards;

    @FindBy(xpath = "//*[@class='board-tile mod-add']")
    private WebElement btnCreateNewBoard;

    public BoardsPage(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {
        waitForUrlContains("boards", 12);
        waitForElementDisplayed(pageBoards, 4);
    }

    @Step("Create new board")
    public BoardWidget createNewBoard() {
        waitForElementDisplayed(btnCreateNewBoard, 4);
        click(btnCreateNewBoard);
        BoardWidget boardWidget = new BoardWidget(browser);
        boardWidget.isOpened();

        return boardWidget;
    }
}
