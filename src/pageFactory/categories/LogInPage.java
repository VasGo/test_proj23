package pageFactory.categories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import core.browsers.Browser;
import ru.yandex.qatools.allure.annotations.Step;

public class LogInPage extends BasePage {

    public LogInPage(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {

    }

    @FindBy(id = "inputEmail")
    private WebElement inputEmail;

    @FindBy(id = "inputPassword")
    private WebElement inputPassword;

    @FindBy(id = "login-submit")
    private WebElement btnSubmit;

    @Step("Open Log in page")
    public void openLogInPage(){
        browser.getDriver().get("/login.htm");
        waitForUrlContains("files_support", 8);
    }

    @Step("Log in via credentials: {0}, {1}")
    public void logIn(String email, String password){
        waitForElementDisplayed(this.inputEmail, 8);
        click(this.inputEmail);
        this.inputEmail.sendKeys(email);

        waitForElementDisplayed(this.inputPassword, 8);
        click(this.inputPassword);
        this.inputPassword.sendKeys(password);

        waitForElementDisplayed(btnSubmit, 8);
        click(btnSubmit);
    }
}
