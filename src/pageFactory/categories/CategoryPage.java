package pageFactory.categories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import core.browsers.Browser;

import java.util.ArrayList;
import java.util.List;

public class CategoryPage extends BasePage {

    @FindBy(xpath = "//div[contains(@class, 'cat-icon')]/span")
    private List<WebElement> files;

    public CategoryPage(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {

    }

    public List<String> getFilesNames(){
        List<String> folder = new ArrayList<>();

        for (WebElement file : files) {
            waitForElementDisplayed(file, 8);
            folder.add(file.getText());
        }

        return folder;
    }
}
