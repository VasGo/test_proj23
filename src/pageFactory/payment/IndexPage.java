package pageFactory.payment;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import core.browsers.Browser;
import ru.yandex.qatools.allure.annotations.Step;

public class IndexPage extends BasePage {

    public IndexPage(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {

    }

    @FindBy(xpath = "//li[@class='dropdown Pret three-items ']")
    WebElement loans;

    @FindBy(xpath = "//a[@data-utag-name='mortgage_loan']")
    WebElement mortgages;

    @Step("Click on Loans")
    public IndexPage clickOnLoans(){
        waitForElementDisplayed(loans, 6);
        click(loans);

        return this;
    }

    @Step("Click on Mortgages")
    public MortgagePage clickOnMortgages(){
        waitForElementDisplayed(mortgages, 6);
        click(mortgages);

        return new MortgagePage(browser);
    }

}
