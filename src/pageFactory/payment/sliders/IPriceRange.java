package pageFactory.payment.sliders;

public interface IPriceRange {

    int getCurrentPrice();

    IPriceRange changePrice(int price) throws InterruptedException;

}
