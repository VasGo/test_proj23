package pageFactory.payment.sliders;

import core.browsers.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.payment.MortgagePaymentCalculator;
import ru.yandex.qatools.allure.annotations.Step;
import utils.tools.Log;

public class UpPurchasePrice extends MortgagePaymentCalculator implements IPriceRange {

    public UpPurchasePrice(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {

    }

    @FindBy(id = "sliderFond")
    WebElement textPrice;

    @FindBy(id = "price_up")
    WebElement btnReducePrice;

    @Override
    public int getCurrentPrice() {
        int price = 0;

        textPriceCurrent = textPrice;
        waitForElementDisplayed(textPriceCurrent, 6);
        try {
            price = Integer.parseInt(getAttribute(textPriceCurrent, "data-value"));
        } catch (NumberFormatException e) {
            Log.error("Cannot get current price in UpPurchasePrice");
            Log.error(e.getMessage());
        }

        return price;
    }

    @Step("Increase price to {0}")
    @Override
    public IPriceRange changePrice(int price) {
        btnPriceChange = btnReducePrice;

        int currentPrice;
        currentPrice = getCurrentPrice();
        scrollToElement(btnPriceChange);
        waitForElementClickable(btnPriceChange, 6);

        while (currentPrice != price) {
            click(btnPriceChange);
            currentPrice = getCurrentPrice();
        }

        return this;
    }
}
