package pageFactory.payment.sliders;

import core.browsers.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.payment.MortgagePaymentCalculator;
import ru.yandex.qatools.allure.annotations.Step;
import utils.tools.Log;

public class DownPurchasePrice extends MortgagePaymentCalculator implements IPriceRange {

    public DownPurchasePrice(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {

    }

    @FindBy(id = "price")
    WebElement textPrice;

    @FindBy(id = "price_reduce")
    WebElement btnReducePrice;

    @Override
    public int getCurrentPrice() {
        int price = 0;

        textPriceCurrent = btnReducePrice;
        waitForElementDisplayed(textPriceCurrent, 6);
        try {
            price = Integer.parseInt(getAttribute(textPriceCurrent, "data-value"));
        } catch (NumberFormatException e) {
            Log.error("Cannot get current price in DownPurchasePrice");
            Log.error(e.getMessage());
        }

        return price;
    }

    @Override
    @Step("Reduce price to {0}")
    public IPriceRange changePrice(int price) {
        btnPriceChange = btnReducePrice;
        int currentPrice;
        currentPrice = getCurrentPrice();

        scrollToElement(btnPriceChange);
        waitForElementClickable(btnPriceChange, 6);

        while (currentPrice != price) {
            click(btnPriceChange);
            currentPrice = getCurrentPrice();
        }

        return this;
    }

}
