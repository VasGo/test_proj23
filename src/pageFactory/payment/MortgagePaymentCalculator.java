package pageFactory.payment;

import core.browsers.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import ru.yandex.qatools.allure.annotations.Step;
import utils.tools.Log;

import java.util.List;

public class MortgagePaymentCalculator extends BasePage {

    public MortgagePaymentCalculator(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {

    }

    protected WebElement textPriceCurrent;
    protected WebElement btnPriceChange;

    @FindBy(xpath = "//*[@class = 'slider-track']")
    List<WebElement> sliders;

    @FindBy(id = "TauxInteret")
    WebElement btnInterestRate;

    @FindBy(xpath = "//*[@class='col-med-1-2 col-lg-1-3']/descendant::*[contains(@class, 'amor')]")
    WebElement btnAmortization;

    @FindBy(xpath = "//li[contains(text(), 'years')]")
    List<WebElement> listAmortization;

    @FindBy(xpath = ".//*[@id='form_calculateur_versements']/div[5]")
    WebElement btnPaymentFrequency;

    @FindBy(xpath = "//*[contains(text(), 'Monthly')]/ancestor::ul[1]/li")
    List<WebElement> listPaymentFrequency;

    @FindBy(id = "btn_calculer")
    WebElement btnCalculate;

    @FindBy(id = "p-resultats")
    WebElement textPriceResult;

    @FindBy(xpath = "//li[contains(text(), 'years') and contains (text(), years)]")
    WebElement selectYear;

    public double getTextPriceResult() {
        String price;
        double result = 0;

        waitForElementDisplayed(textPriceResult, 6);
        scrollToElement(textPriceResult);
        price = textPriceResult.getText();
        try {
            price = price.replace("$ ", "");
            if (price.contains(",")) {
                price = price.replace(",", "");
            }
            result = Double.parseDouble(price);
        } catch (NumberFormatException e) {
            Log.error("Cannot parse price result to double");
            Log.error(e.getMessage());
        }

        return result;
    }

    @Step("Select years: {0}")
    public MortgagePaymentCalculator selectYears(int years) {
        waitForElementClickable(btnAmortization, 4);
        click(btnAmortization);
        click(selectYear);

        return this;
    }

    @Step("Select payment Frequency: {0}")
    public MortgagePaymentCalculator selectPaymentFrequency(String frequency) {
        waitForElementClickable(btnPaymentFrequency, 4);
        click(btnPaymentFrequency);

        listPaymentFrequency.forEach(element -> {
            if (element.getText().trim().equals(frequency)) {
                waitForElementDisplayed(element, 6);
                waitForElementClickable(element, 6);
                click(element);
            }
        });

        return this;
    }

    @Step("Input interest rate: {0}")
    public MortgagePaymentCalculator inputInterestRate(int rate) {
        waitForElementClickable(btnInterestRate, 4);
        click(btnInterestRate);
        btnInterestRate.clear();
        btnInterestRate.sendKeys(String.valueOf(rate));

        return this;
    }

    @Step("Click btnCalculate")
    public MortgagePaymentCalculator clickCalculate() {
        waitForElementClickable(btnCalculate, 4);
        click(btnCalculate);

        return this;
    }

}
