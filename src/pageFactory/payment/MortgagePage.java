package pageFactory.payment;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import core.browsers.Browser;
import ru.yandex.qatools.allure.annotations.Step;

public class MortgagePage extends BasePage {

    public MortgagePage(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {

    }

    @FindBy(xpath = "//*[@class= 'light bottom-10']/a[contains(@href, '/mo')]")
    WebElement btnCalculateYourPayment;

    @Step("Click calculate your payment")
    public MortgagePaymentCalculator clickCalculateYourPayment(){
        waitForElementClickable(btnCalculateYourPayment, 6);
        click(btnCalculateYourPayment);

        return new MortgagePaymentCalculator(browser);
    }
}
