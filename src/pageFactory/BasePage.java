package pageFactory;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;
import core.browsers.Browser;
import org.openqa.selenium.support.ui.Duration;
import ru.yandex.qatools.allure.annotations.Step;
import utils.tools.Log;
import utils.tools.TimeTool;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public abstract class BasePage {

    protected WebDriver driver;
    protected Browser browser;

    public BasePage(Browser browser) {
        this.browser = browser;
        driver = browser.getDriver();
        PageFactory.initElements(driver, this);
    }

    public abstract void isOpened();

    public void openURL(String url) {
        try {
            driver.get(url);
        } catch (TimeoutException e) {
            Log.error("TimeoutException: Cannot open url: " + url + " Error message: " + e.getMessage());
        }
    }

    /**
     * Method will return true if url contains text due to waitingTime otherwise false will be returned
     *
     * @param text          expected url piece
     * @param timeInSeconds waiting time
     * @return boolean contains or not
     */
    public boolean waitForUrlContains(String text, int timeInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeInSeconds);
            return wait.until(ExpectedConditions.urlContains(text));
        } catch (TimeoutException e) {
            Log.error("Url does not contains the text: " + text);
            Log.error("TimeoutException: " + e.getMessage());
            return false;
        }
    }

    public boolean waitForUrl(String url, int timeInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeInSeconds);
            return wait.until(ExpectedConditions.urlToBe(url));
        } catch (TimeoutException e) {
            Log.error("Url is not present: " + url);
            Log.error("TimeoutException: " + e.getMessage());
            return false;
        }
    }

    /**
     * Wait for title contains any text
     *
     * @param titleText         String text whats need contains
     * @param waitTimeInSeconds wait time for seconds
     * @return boolean, is title contains titleText
     */
    public boolean titleContains(String titleText, int waitTimeInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitTimeInSeconds);
            return wait.until(ExpectedConditions.titleContains(titleText));
        } catch (TimeoutException e) {
            Log.error("Title does not contains the text: " + titleText);
            Log.error("TimeoutException: " + e.getMessage());
            return false;
        }
    }

    /**
     * Wait for text present inside element
     *
     * @param element           WebElement where text needs to be present
     * @param text              String text which need to be present in element
     * @param waitTimeInSeconds max waiting time in seconds
     * @return boolean, is text present inside element
     */
    public boolean waitForTextPresent(WebElement element, String text, int waitTimeInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitTimeInSeconds);
            return wait.until(ExpectedConditions.textToBePresentInElement(element, text));
        } catch (TimeoutException e) {
            Log.error("Text is not present: " + text + " in element " + element);
            Log.error("TimeoutException: " + e.getMessage());
            return false;
        }
    }

    /**
     * Method Check if element is present on page
     *
     * @param element WebElement
     * @return true or false
     */
    public boolean isElementPresentOnPage(WebElement element) {
        boolean isPresent = false;

        try {
            if (element.isDisplayed())
                isPresent = true;
        } catch (ElementNotVisibleException e) {
            isPresent = false;
            Log.error("Element is not visible: " + element);
            Log.error("ElementNotVisibleException: " + e.getMessage());
        }

        return isPresent;
    }

    /**
     * Method return boolean is the element is clicked after (int) seconds
     *
     * @param element what needs to be clicked
     * @param seconds wait time
     * @return boolean element is clicked or not
     */
    public boolean waitForElementClickable(WebElement element, int seconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            return true;
        } catch (WebDriverException e) {
            Log.error("Element is not clickable: " + element);
            Log.error("WebDriverException: " + e.getMessage());
            return false;
        } catch (Exception e) {
            Log.error("Element is not clickable: " + element);
            Log.error("Exception: " + e.getMessage());
            return false;
        }
    }

    public boolean isElementPresentOnPageOnPageXpath(String xpath, Object... args) {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(By.xpath(String.format(xpath, args)));
        return elements.size() > 0;
    }

    /**
     * Method return boolean if element will be present after (int) seconds
     *
     * @param element       whats u want to find
     * @param timeInSeconds time of search
     * @return boolean true -  if find, false - if not find
     */
    public boolean waitForElementDisplayed(WebElement element, int timeInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeInSeconds);
            return wait.until(ExpectedConditions.visibilityOf(element)).isDisplayed();
        } catch (NoSuchElementException e) {
            Log.error("Element is not visible: " + element);
            Log.error("NoSuchElementException: " + e.getMessage());
            return false;
        } catch (Exception e) {
            Log.error("Element is not visible: " + element);
            Log.error("Exception: " + e.getMessage());
            return false;
        }
    }

    /**
     * Method return boolean if elements will be present after (int) seconds
     *
     * @param elementsList  BoardList<WebElement> elements list, whats u want to find
     * @param timeInSeconds time for waiting
     * @return boolean true -  if find, false - if not find
     */
    public boolean waitForElementDisplayed(List<WebElement> elementsList, int timeInSeconds) {
        for (WebElement element : elementsList) {
            if (waitForElementDisplayed(element, timeInSeconds))
                return true;
        }

        return false;
    }

    /**
     * method check that element is visible in the DOM and on the page
     *
     * @param by            - element locator
     * @param timeInSeconds
     */
    public boolean waitForElementVisibility(By by, int timeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeInSeconds);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            return true;
        } catch (TimeoutException e) {
            Log.error("Element is not visible: " + by);
            Log.error("TimeOutException: " + e.getMessage());
            return false;
        }
    }

    public boolean waitForElementInvisibile(String xpath, int waitInSecondsBefore) {
        driver.manage().timeouts().implicitlyWait(waitInSecondsBefore, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, waitInSecondsBefore);
        return wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
    }

    /**
     * wait that element is present and clickable before click
     */
    public void waitAndClick(WebElement element, int time) {
        waitForElementDisplayed(element, time);
        waitForElementClickable(element, time);
        this.click(element);
    }

    /**
     * Advanced WebDriver webElement.click() action,
     * is verify, is element is present, and if true, click on it. Or throw exception.
     *
     * @param element WebElement what is needed to be clicked
     * @throws WebDriverException
     */
    public void click(WebElement element) {
        if (isElementPresentOnPage(element)) {
            element.click();
        } else {
            throw new WebDriverException(element.getTagName() + "\t" + element.getAttribute("class") + "is not clickable");
        }
    }

    public WebElement getParentElementOf(WebElement element) {
        return element.findElement(By.xpath(".."));
    }

    public void hoverOnElement(WebElement element) {
        Actions action = new Actions(driver);

        try {
            action.build();
            action.moveToElement(element).perform();
        } catch (Exception e) {
            Log.error("Element is not visible: " + element);
            Log.error("Exception: " + e.getMessage());
        }
    }

    public void clickAndHold(WebElement element) {
        Actions action = new Actions(driver);

        try {
            action.clickAndHold(element).build().perform();
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
        }
    }

    /**
     * Method Take ScreenShot of current page
     *
     * @param screenName name of screenShot
     * @return absolute path to screenShot
     * @throws IOException
     */
    public String takeScreenShot(String screenName) throws IOException {
        String screenDirectory = "E:/resources/TestReports/";
        String absolutePath = "";
        screenName = screenName + ".png";

        File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        absolutePath = screenDirectory + "/" + screenName;
        FileUtils.copyFile(screenShot, new File(absolutePath));
        return absolutePath;
    }

    public void type(String text) {
        Actions actions = new Actions(driver);
        TimeTool.sleep(1);
        actions.sendKeys(text).build().perform();
        Log.info("Type text: " + text);
    }

//    @Step("DoubleClick on ({0})")
//    protected void doubleClick(By locator) {
//        Wait wait = new FluentWait(driver)
//                .withTimeout(Duration.ofSeconds(15))
//                .pollingEvery(Duration.ofMillis(500))
//                .ignoring(ElementNotVisibleException.class).ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);
//        try {
//            wait.until(ExpectedConditions.elementToBeClickable(locator));
//            Log.info("DoubleClicking on '" + locator + "'");
//            Actions action = new Actions(driver);
//            WebElement btnElement = driver.findElement(locator);
//            action.doubleClick(btnElement).build().perform();
//
//        } catch (TimeoutException e) {
//            Log.error("Can not doubleclick on element '" + locator + "'");
//        }
//    }

    @Step("JSHelper: click on element [{0}]")
    public void clickJS(By locator) {
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }

    protected void refreshPage() {
        Log.info("Refreshing the page");
        driver.navigate().refresh();
    }

    protected void hoverOver(By locator) {
        WebElement element = driver.findElement(locator);
        Actions builder = new Actions(driver);
        builder.moveToElement(element);
        TimeTool.sleep(1);
        builder.build().perform();
    }

    public String getAttribute(WebElement element, String attribute) {
        String attributeValue = "Attribute is not present";

        try {
            attributeValue = element.getAttribute(attribute);
        } catch (WebDriverException e) {
            Log.error("Attribute " + attribute + " is not presented: " + element);
            Log.error(e.getMessage());
        }

        return attributeValue;
    }

    /**
     * Execute Javascript command
     *
     * @param script string javascript code
     */
    public JavascriptExecutor executeJS(String script) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(script);

        return js;
    }

    public void scrollToElement(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public boolean waitForPageLoad() {
        int time = 60;
        try {
            WebDriverWait wait = new WebDriverWait(driver, time, 50);
            wait.until((ExpectedCondition<Boolean>) d -> ((JavascriptExecutor) d).executeScript("return document.readyState").equals("complete"));
            return true;
        } catch (TimeoutException e) {
            Log.error("Page is not loaded: " + driver.getCurrentUrl());
            return false;
        }
    }
}
