package pageFactory.booking;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import core.browsers.Browser;
import ru.yandex.qatools.allure.annotations.Step;

import java.time.Month;
import java.util.List;

public class BookingPage extends BasePage {

    public BookingPage(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {

    }

    @FindBy(xpath = "(//*[@id='ss'])[1]")
    private WebElement elementSearchCity;

    @FindBy(xpath = "//*[contains(@class, 'c-autocomplete__list sb-autocomplete__list -visible') or contains(@class, 'c-autocomplete__list')]/li")
    private List<WebElement> dropdownListOfCities;

    @FindBy(xpath = "//*[@id='basiclayout']/descendant::*[@class='sb-dates__col --checkin-field']")
    private WebElement btnDateStart;

    @FindBy(xpath = "//*[@id='basiclayout']/descendant::*[@class='sb-dates__col --checkout-field']")
    private WebElement btnDateEnd;

    @FindBy(xpath = "//*[@class='c2-month-header-monthname']")
    private List<WebElement> headerMonth;

    @FindBy(xpath = "(//*[@aria-label='Go forward to next month'])[1]")
    private WebElement nextMonthForStartDate;

    @FindBy(xpath = "(//*[@aria-label='Go forward to next month'])[2]")
    private WebElement nextMonthForEndDate;

    @FindBy(xpath = "//span[@class='c2-day-inner']")
    private List<WebElement> listDays;

    @FindBy(xpath = "//*[@class='sb-searchbox__button   ']")
    private WebElement btnStartSearch;

    @FindBy(xpath = "//*[@class='gta-widget-input flag-indent ']")
    private WebElement phoneNumber;

    @FindBy(xpath = "//*[@class='b-button__text']")
    private WebElement submitPhoneNumber;

    @FindBy(xpath = "//*[contains(@class, 'gta-widget-message invalid') or contains(@class, 'gta-widget-message success')]")
    private WebElement outputMobilePhoneMessage;

    @Step("Input city: {0}")
    public void inputCity(String city) {
        waitForElementDisplayed(elementSearchCity, 8);
        hoverOnElement(elementSearchCity);
        click(elementSearchCity);
        elementSearchCity.sendKeys(city);

        // select first city from dropdown list by default
        waitForElementDisplayed(dropdownListOfCities.get(0), 8);
        click(dropdownListOfCities.get(0));
    }

    @Step("Select values in calendar. Year: {0}, startMonth: {1}, endMonth:{2}, startDay: {3}, endDay: {4}")
    public void bookTrip(
            int year,
            Month startMonth,
            Month endMonth,
            int startDayInMonth,
            int endDayInMonth
    ) {

        boolean calendar = true;

        // select start date of trip
        waitForElementDisplayed(btnDateStart, 8);
        this.clickAndHold(btnDateStart);

        // user search day in calendar of check in
        for (int i = 0; i < headerMonth.size() && calendar; i++) {
            if (headerMonth.get(i).isDisplayed()) {
                if (headerMonth.get(i).getText().contains(startMonth.name()) && headerMonth.get(i).getText().contains(Integer.toString(year)) && headerMonth.get(i).isDisplayed()) {
                    for (int day = 0; day < listDays.size() && calendar; day++) {
                        if (listDays.get(day).isDisplayed() && Integer.parseInt(listDays.get(day).getText().trim()) == startDayInMonth) {
                            this.click(listDays.get(day));
                            calendar = false;
                        }
                    }
                } else {
                    if (calendar == true) {
                        waitForElementDisplayed(nextMonthForStartDate, 8);
                        this.click(nextMonthForStartDate);
                    }
                }
            }
        }
        // // select end date of trip
        this.click(btnDateEnd);
        calendar = true;
        headerMonth.clear();
        // user search day in calendar of check out
        for (int j = 0; j < headerMonth.size() && calendar == true; j++) {
            if (headerMonth.get(j).isDisplayed()) {
                if (headerMonth.get(j).getText().contains(endMonth.name()) && headerMonth.get(j).getText().contains(Integer.toString(year)) && headerMonth.get(j).isDisplayed()) {
                    for (int day = 0; day < listDays.size() && calendar == true; day++) {
                        if (listDays.get(day).isDisplayed() && Integer.parseInt(listDays.get(day).getText().trim()) == endDayInMonth) {
                            click(listDays.get(day));
                            calendar = false;
                        }
                    }
                } else {
                    if (calendar == true) {
                        waitForElementDisplayed(nextMonthForEndDate, 8);
                        click(nextMonthForEndDate);
                    }
                }
            }
        }

        waitForElementDisplayed(btnStartSearch, 8);
        click(btnStartSearch);
    }

    @Step("Input phone number: {0}")
    public void inputPhoneNumber(String phoneNumber) {
        scrollToElement(this.phoneNumber);
        waitForElementClickable(this.phoneNumber, 8);
        click(this.phoneNumber);
        this.phoneNumber.sendKeys(phoneNumber);
        click(submitPhoneNumber);
    }

    public WebElement getOutputMobilePhone() {
        waitForElementDisplayed(outputMobilePhoneMessage, 4);
        return outputMobilePhoneMessage;
    }

    public String getOutputMobilePhoneMessage() {
        waitForElementDisplayed(outputMobilePhoneMessage, 4);
        return outputMobilePhoneMessage.getText();
    }

    @Step("Delete current phone number")
    public void deleteCurrentPhoneNumber() {
        click(phoneNumber);
        phoneNumber.clear();
    }

}
