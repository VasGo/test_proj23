package pageFactory.booking;

import core.browsers.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageFactory.BasePage;
import utils.tools.Log;

import java.util.List;

public class ListHotels extends BasePage {

    public ListHotels(Browser browser) {
        super(browser);
    }

    @Override
    public void isOpened() {

    }

    @FindBy(xpath = "//*[@class=' jq_tooltip  district_link visited_link  ']")
    private List<WebElement> listHotels;

    public boolean checkHotelsInCity(String city) throws InterruptedException {
        boolean cityType = true;
        waitForElementDisplayed(listHotels, 8);

        for (WebElement hotel : listHotels) {
            if (!hotel.getText().contains(city)) {
                cityType = false;
                Log.error("Invalid hotel text is " + hotel.getText());
                break;
            }
        }

        return cityType;
    }
}
