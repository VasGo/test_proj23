package utils.hosts;

import java.net.MalformedURLException;
import java.net.URL;

public class HostConfig {

    public static URL setHost(Host host) throws MalformedURLException {
        String hostAddress = "";

        switch (host) {
            case GRID:
                hostAddress = host.GRID.getIpHost();
                break;
            case LOCALHOST:
                hostAddress = host.LOCALHOST.getIpHost();
                break;
        }

        return new URL(hostAddress);
    }
}
