package utils.hosts;

import java.net.MalformedURLException;
import java.net.URL;

public enum Host {

    LOCALHOST("http://127.0.0.1:4444/wd/hub"),
    GRID("http://ip_address:4444/wd/hub");

    private String ipHost = "";

    Host(String name) {
        this.ipHost = ipHost;
    }

    public String getIpHost() {
        return ipHost;
    }

    public static URL setHost(Host host) throws MalformedURLException {
        String hostAddress = "";

        switch (host) {
            case GRID:
                hostAddress = host.GRID.getIpHost();
                break;
            case LOCALHOST:
                hostAddress = host.LOCALHOST.getIpHost();
                break;
        }

        return new URL(hostAddress);
    }
}
