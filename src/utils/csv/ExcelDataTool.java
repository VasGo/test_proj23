package utils.csv;

import com.opencsv.CSVWriter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class ExcelDataTool {

    private String dirCSV = "resources/filesExcel/";
    String formatForFile = ".csv";

    public ExcelDataTool() {

    }

    public ExcelDataTool(String nameCVS) {
        this.dirCSV = dirCSV + nameCVS;
    }

    public Object[][] parseRow(
            String sheetName,
            int numberRow,
            int elementsRow
    ) throws IOException, InvalidFormatException {
        Object[][] tableRow = new Object[elementsRow][1];

        Workbook excel = WorkbookFactory.create(new File(this.dirCSV));
        Sheet sheet = excel.getSheet(sheetName);
        Row row = sheet.getRow(numberRow - 1);

        try {
            for (int i = numberRow; i <= elementsRow; i++) {
                tableRow[i - 1][0] = row.getCell(i - 1).getRichStringCellValue().toString();
            }
        } catch (NumberFormatException e) {
            System.err.print("NumberFormatException: " + e.getMessage());
        }

        return tableRow;
    }

    public Object[][] parseColumn(
            String sheetName,
            int numberColumn,
            int rowStart,
            int rowEnd
    ) throws IOException, InvalidFormatException {
        Object[][] tableColumn = new Object[rowEnd - rowStart + 1][1];

        Workbook excel = WorkbookFactory.create(new File(this.dirCSV));
        Sheet sheet = excel.getSheet(sheetName);
        Row row;

        for (int i = rowStart; i <= rowEnd; i++) {
            row = sheet.getRow(i - 1);
            tableColumn[i - rowStart][0] = row.getCell(numberColumn - 1).getRichStringCellValue().toString();
        }

        return tableColumn;
    }

    public String parseCell(
            String sheetName,
            int numberRow,
            int numberColumn
    ) throws IOException, org.apache.poi.openxml4j.exceptions.InvalidFormatException {
        Workbook excel = WorkbookFactory.create(new File(this.dirCSV));
        Sheet sheet = excel.getSheet(sheetName);
        Row row = sheet.getRow(numberRow);
        Cell column = row.getCell(numberColumn);

        return column.getRichStringCellValue().toString();
    }

    // method is used in DataProvider
    public Object[][] parseTable(
            String sheetName,
            int columnStart,
            int columnEnd,
            int rowStart,
            int rowEnd
    ) throws IOException, org.apache.poi.openxml4j.exceptions.InvalidFormatException {
        Object[][] table = new Object[rowEnd][columnEnd];

        Workbook excel = WorkbookFactory.create(new File(this.dirCSV));
        Sheet sheet = excel.getSheet(sheetName);
        Row row;

        try {
            for (int j = rowStart; j <= rowEnd; j++) {
                row = sheet.getRow(j - 1);
                for (int i = columnStart; i <= columnEnd; i++) {
                    table[j - 1][i - 1] = row.getCell(i - 1).getRichStringCellValue().toString();
                }
            }
        } catch (NumberFormatException e) {
            System.err.print("NumberFormatException: " + e.getMessage());
        }

        return table;
    }

    /**
     * Create CSV file if not exist;
     *
     * @param name of csv file, without format
     * @return absolute path of csv file;
     */
    public String createCsv(String name) {
        String csvPath = "resources/reports/" + name + formatForFile;
        new File(csvPath);
        return csvPath;
    }

    public void writeToCsv(String csvFile, HashMap<String, String> data) throws IOException {
        CSVWriter writer = new CSVWriter(new FileWriter(csvFile, true), ',', CSVWriter.NO_QUOTE_CHARACTER);
        String[] array = data.values().toArray(new String[data.size()]);
        writer.writeNext(array);
        writer.close();
    }

}
