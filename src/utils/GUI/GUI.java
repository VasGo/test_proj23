package utils.GUI;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Aleksey123 on 5/21/2017.
 */
public class GUI {

    WebDriver driver;

    public String getTextColor(WebElement element){
        return element.getCssValue("color");
    }

    public String getTextSize(WebElement element){
        return element.getCssValue("font-size");
    }
}
