package utils.tools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class ScreenShotTool {

    WebDriver driver;

    public ScreenShotTool(WebDriver driver) {
        this.driver = driver;
    }

    public Screenshot takeScreenshot(Set<By> ignoredElements) {
        AShot aShot = new AShot();
        try {
            if (ignoredElements != null && ignoredElements.size() != 0) {
                return aShot.shootingStrategy(new ViewportPastingStrategy(100)).ignoredElements(ignoredElements).takeScreenshot(driver);
            } else {
                return aShot.shootingStrategy(new ViewportPastingStrategy(100)).takeScreenshot(driver);
            }
        } finally {
            driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
        }
    }

    public ImageDiff getImageDiff(Screenshot expected, Screenshot actual) {
        Log.info("Making diff of 2 screenshots");
        expected.setIgnoredAreas(actual.getIgnoredAreas());
        return new ImageDiffer().makeDiff(expected, actual);
    }

    @Step("Take screenshot of element [{0}]")
    public Screenshot takeScreenshotOfElement(WebElement element) {
        Log.info("AShot: taking screenshot of element [" + element + "]");
        AShot aShot = new AShot();
        try {
            return aShot.takeScreenshot(driver, element);
        } finally {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }

    public Screenshot takeScreenshotWithOutJquery(WebElement element) {
        return new AShot().coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(driver, element);
    }

    public Screenshot getImage(String path) throws IOException {
        BufferedImage image = ImageIO.read(new File(path));
        return new Screenshot(image);
    }

    public void saveImage(Screenshot screenshot, String filepath) throws IOException {
        ImageIO.write(screenshot.getImage(), "png", new File(filepath));
    }
}
