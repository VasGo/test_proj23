package utils.tools;

import org.apache.commons.lang3.text.WordUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class RandomTool {

    Random randomGenerator = new Random();
    private static int longRandAdd = 0;

    /**
     * @return YYYY-MM-DD_HHMMSS
     */
    public String getRandomDate() {
        return new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
    }

    public boolean randomBool() {
        return randomGenerator.nextBoolean();
    }

    public String getDateAndTime() {
        return new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    public String getTime(String separator) {
        return new SimpleDateFormat("HH" + separator + "mm" + separator + "ss").format(Calendar.getInstance().getTime());
    }

    public String getSeconds() {
        return new SimpleDateFormat("mmssS").format(Calendar.getInstance().getTime());
    }

    public int getUniqueNumWithMaxParam(int max) {
        return randomGenerator.nextInt(max);
    }

    /**
     * @param from int
     * @param to   int
     * @return range int (from - to)
     */
    public int randomNumber(int from, int to) {
        return randomGenerator.nextInt((to - from) + 1) + from;
    }

    /**
     * @return YYYY-MM-DD
     */
    public String getDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    }

    /**
     * Create random group of symbols
     *
     * @return random screenName (group of symbols) with 7 length.
     */
    public String randomScreenName() {
        String alphabet = "abcdefghijklmnopqrstyvwxyz";
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < 7; i++) {
            result.append(alphabet.charAt(randomNumber(0, 25)));
        }

        return WordUtils.capitalize(result.toString());
    }

    public String randomEmail(String email) {
        String[] split = email.split("@");
        String first = split[0] + "+" + getSeconds() + randomNumber(100, 999);

        return first + "@" + split[1];
    }

    public static int genInt(int from, int to) {
        int tmp = 0;
        if (to >= from)
            tmp = (int) (from + Math.round((Math.random() * (to - from))));

        return tmp;
    }

    public static String generateString(int length) {
        String srandomString = "";
        for (int i = 0; i < length; i++) {
            srandomString = srandomString + Character.toString((char) RandomTool.genInt(97, 120));
        }

        return srandomString;
    }
}
