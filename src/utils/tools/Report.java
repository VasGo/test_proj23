package utils.tools;

import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Report {

    @Attachment(value = "{0}", type = "video/avi")
    public static byte[] addVideo(String name, String path) {
        return getBytesFromFile(path);
    }

    @Attachment(value = "{0}", type = "plain/text")
    public static String addText(String name, String text) {
        return text;
    }

    @Attachment(value = "{0}", type = "image/png")
    public static byte[] addImage(String name, String path) {
        return getBytesFromFile(path);
    }

    @Attachment(value = "{1}", type = "application/json")
    public static String addJSON(String json, String name) {
        return json;
    }

    private static byte[] getBytesFromFile(String filePath) {
        byte[] fileAsByteArray = "".getBytes();
        try {
            fileAsByteArray = Files.readAllBytes(Paths.get(filePath));
        } catch (IOException e) {
            Log.error(e.getMessage());
        }
        return fileAsByteArray;
    }

}