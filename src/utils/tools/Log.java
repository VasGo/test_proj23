package utils.tools;

public class Log {

    private static String dateFormat = "dd-mm-yy HH:mm:ss";

    public static void info(String event) {
        String time = TimeTool.getCurrentDate(dateFormat);
        System.out.println("[INFO] " + time + " " + event);
    }

    public static void error(String event) {
        String time = TimeTool.getCurrentDate(dateFormat);
        System.err.println("[ERROR] " + time + " " + event);
    }

    public static void warning(String event) {
        String time = TimeTool.getCurrentDate(dateFormat);
        System.out.println("[WARNING] " + time + " " + event);
    }
}