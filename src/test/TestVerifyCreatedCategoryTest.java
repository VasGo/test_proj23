import core.browsers.BrowserType;
import org.apache.http.NameValuePair;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageFactory.categories.CategoryPage;
import pageFactory.categories.LogInPage;
import core.httpClient.CategoriesAPI;
import core.browsers.Browser;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class TestVerifyCreatedCategoryTest {

    private Browser browser;
    private LogInPage user;
    private CategoryPage category;
    private CategoriesAPI http;
    private List<NameValuePair> file;
    private String fileName="uc_export=download_id=0B4kCsWsTSqMBZ0k0d2VnZFJnWGc";

    @Parameters({"browserName"})
    @BeforeClass
    public void initBrowser(BrowserType browserName){
        browser = new Browser(browserName);
        browser.openLocaleBrowser();
        http = new CategoriesAPI("user_API_key");
        file = new ArrayList<>();
        user = new LogInPage(browser);
        category = new CategoryPage(browser);
    }

    @Test
    public void testUploadFileFromGoogleDrive(){
        String filePath = "https://drive.google.com/uc?export=download&id=0B4kCsWsTSqMBZ0k0d2VnZFJnWGc";

        file = http.uploadFile(filePath);
        http.post("api_end_point", file);
        http.execute();
    }

    @Test(dependsOnMethods = "testUploadFileFromGoogleDrive")
    public void testThatFileWasUploadedOnFrontEnd(){
        user.openLogInPage();
        user.logIn("testemail+4454545@gmail.com", "PASdfSword123dsf4");

        assertTrue(category.getFilesNames().contains(fileName), "File was not uploaded on frontend");
    }

    @AfterClass
    public void closeBrowser(){browser.close();}
}
