import core.browsers.BrowserType;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageFactory.categories.CategoryPage;
import pageFactory.categories.LogInPage;
import core.httpClient.CategoriesAPI;
import core.browsers.Browser;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class TestVerifyAllCategoriesNamesTest {

    private Browser browser;
    private LogInPage user;
    private CategoryPage category;
    private CategoriesAPI http;
    private JSONObject json;

    @Parameters({"browserName"})
    @BeforeClass
    public void initBrowser(String browserName) {
        browser = new Browser(BrowserType.valueOf(browserName));
        browser.openLocaleBrowser();
        http = new CategoriesAPI("user_API_key");
        user = new LogInPage(browser);
        category = new CategoryPage(browser);
    }

    @Stories("Get all categories by API call")
    @Test
    public void testOpenAllCategoriesFromAPI() throws JSONException {
        http.get("api_end_point");
        http.execute();
        json = http.getJSONresponse();
    }

    @Stories("Open all categories on FrontEnd")
    @Test(dependsOnMethods = "testOpenAllCategoriesFromAPI")
    public void testVerifyAllCategoriesNamesOnFrontEndWithAPI() throws JSONException {
        List<String> categoriesOnFrontEnd;
        List<String> categoriessAPI;

        user.openLogInPage();
        user.logIn("testemail+4454545@gmail.com", "PASdfSword123dsf4");
        // get all categories names on FrontEnd
        categoriesOnFrontEnd = category.getFilesNames();
        // get all categories names from API
        categoriessAPI = http.getCategoriesWithOutFormats(json);

        for (String category : categoriesOnFrontEnd) {
            assertTrue(categoriessAPI.contains(category), "Wrong category on frontend: " + category);
        }
    }

    @AfterClass
    public void close() {
        browser.close();
    }
}
