import core.browsers.Browser;
import core.browsers.BrowserType;
import org.testng.annotations.*;
import pageFactory.booking.BookingPage;
import ru.yandex.qatools.allure.annotations.Features;
import utils.GUI.GUI;

import static org.testng.Assert.assertEquals;

@Features("Invalid phones numbers in phone pop up")
public class InvalidPhoneNumberTest {

    private Browser browser;
    private GUI gui;
    private BookingPage index;

    @Parameters({"browserName"})
    @BeforeClass
    public void initBrowser(BrowserType browserName){
        // init browser
        browser = new Browser(browserName);
        browser.openLocaleBrowser();
        index = new BookingPage(browser);
        gui = new GUI();
    }

    @DataProvider(name = "mobileNumbers")
    public Object[][] dataProvider(){
        return new Object[][]{
                {" 0976543445"},
                {"0966543445 "},
                {"068-654-34-45"},
                {"050 654 00 45"},
                {"sdf0976543445"},
                {"0676543445ghg"},
                {"09365434450"},
                {"063654344"},
                {"sdfsdfsfsfsdf"},
                {""},
        };
    }

    @Test(dataProvider = "mobileNumbers")
    public void testInvalidPhoneNumbers(String phoneNumber){
        index.inputPhoneNumber(phoneNumber);
        // check text of error message
        assertEquals(
                index.getOutputMobilePhoneMessage(),
                "Please make sure your number is correct and try again.",
                "Wrong invalid phone number"
        );
        // check that color of error message is red
         assertEquals("rgba(226, 17, 17, 1)", gui.getTextColor(index.getOutputMobilePhone()), "Wrong color of invalid number");
    }

    @AfterMethod
    public void deleteCurrentPhoneNumber(){
        index.deleteCurrentPhoneNumber();
    }

    @AfterClass
    public void closeBrowser(){
        browser.close();
    }

}