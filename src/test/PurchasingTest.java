import core.browsers.Browser;
import core.browsers.BrowserType;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageFactory.payment.IndexPage;
import pageFactory.payment.MortgagePage;
import pageFactory.payment.MortgagePaymentCalculator;
import pageFactory.payment.sliders.DownPurchasePrice;
import pageFactory.payment.sliders.UpPurchasePrice;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import utils.csv.ExcelDataTool;

import java.io.IOException;
import java.util.HashMap;

import static org.testng.Assert.assertEquals;

@Features("Calculating price in slider")
public class PurchasingTest {

    private Browser browser;
    private IndexPage index;
    private MortgagePaymentCalculator calculator;
    private String reportFile;
    private ExcelDataTool csv;
    private HashMap<String, String> reportData;

    @Parameters({"browserName"}) // browserName is defined in run.xml
    @BeforeClass
    public void initBrowser(BrowserType browserName) {
        // init browser
        browser = new Browser(browserName);
        browser.openLocaleBrowser();

        // init payment
        index = new IndexPage(browser);

        csv = new ExcelDataTool();
        reportFile = csv.createCsv("reportCalculatePayment");
        reportData = new HashMap<>();
        reportData.put("UpPurchasePrice", "UpPurchasePrice: " + "500000");
        reportData.put("DownPrice", "DownPrice: " + "100000");
        reportData.put("Years", "Years: " + "15");
        reportData.put("PaymentFrequency", "PaymentFrequency: " + "weekly");
        reportData.put("InterestRate", "InterestRate: " + "5");
    }

    @Test
    @Stories("Purchase Price is changed after clicking on btn plus")
    public void checkPurchasePriceInFirstSlider() throws InterruptedException {
        // steps
        MortgagePage mortgage = index
                .clickOnLoans()
                .clickOnMortgages();
        calculator = mortgage.clickCalculateYourPayment();
        UpPurchasePrice sliderFirst = new UpPurchasePrice(browser);
        sliderFirst.changePrice(500000);

        // check actual result
        assertEquals(sliderFirst.getCurrentPrice(), 500000, "Wrong price in first slider");
    }

    @Stories("Down Price is changed after clicking on btn plus")
    @Test(dependsOnMethods = "checkPurchasePriceInFirstSlider")
    public void checkDownPriceInSecondSlider() throws InterruptedException {
        // steps
        DownPurchasePrice sliderSecond = new DownPurchasePrice(browser);
        sliderSecond.changePrice(100000);

        // check actual result
        assertEquals(sliderSecond.getCurrentPrice(), 100000, "Wrong price in second slider");
    }

    @Stories("User calculate weekly payments")
    @Test(dependsOnMethods = "checkDownPriceInSecondSlider")
    public void checkResultPrice() throws IOException {
        // steps
        calculator
                .selectYears(15).selectPaymentFrequency("weekly")
                .inputInterestRate(5)
                .clickCalculate();

        // check actual result
        assertEquals(calculator.getTextPriceResult(), 726.35, "Wrong price after calculating");

        // report output data
        reportData.put("Price Result", "Price Result: " + calculator.getTextPriceResult());
        csv.writeToCsv(reportFile, reportData);
    }

    @AfterClass
    public void close() {
        browser.close();
    }
}