import core.browsers.BrowserType;
import org.testng.annotations.*;
import pageFactory.booking.ListHotels;
import pageFactory.booking.BookingPage;
import core.browsers.Browser;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.time.Month;

import static org.testng.Assert.assertTrue;

@Features("User tries to select all hotels in New York")
public class BookingTest {

    private Browser browser;
    private BookingPage index;
    private ListHotels hotels;

    @Parameters({"browserName"})
    @BeforeClass
    public void initBrowser(BrowserType browserName){
        // init browser
        browser = new Browser(browserName);
        browser.openLocaleBrowser();
        index = new BookingPage(browser);
        hotels = new ListHotels(browser);
    }

    @Stories("Use select city option before choosing the calendar")
    @Test
    public void testBookingTrip() throws InterruptedException {
       index.inputCity("New York");
       index.bookTrip(
               2017,
               Month.SEPTEMBER,
               Month.SEPTEMBER,
               20,
               25
       );

       assertTrue(hotels.checkHotelsInCity("New York"), "Wrong city in hotel's list");
    }

    @AfterClass
    public void closeBrowser(){
        browser.close();
    }
}