package trello;

import core.httpClient.trello.TrelloAPI;
import core.httpClient.trello.entities.Board;
import core.httpClient.trello.entities.BoardList;
import core.httpClient.trello.entities.Card;
import core.httpClient.trello.entities.Label;
import org.json.JSONException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.testng.Assert.assertEquals;

public class TrelloAPItest {

    private String nameBoard = "BOARD_TEST8";
    private String list1Name = "list1";
    private String list2Name = "list2";
    private String labelName = "Label";
    private String cardName = "Card";
    private TrelloAPI trelloAPI;
    private Board board;
    private BoardList list1;
    private BoardList list2;
    private Card card;

    @Stories("Create board")
    @Test
    public void testCreateBoard() throws JSONException {
        trelloAPI = new TrelloAPI();
        board = trelloAPI.createBoard(nameBoard);
        assertEquals(board.getName(), nameBoard, "Wrong board name after creation board by API call");
    }

    @Stories("Create list")
    @Test(dependsOnMethods = "testCreateBoard")
    public void testCreateList() throws JSONException {
        SoftAssert softAssert = new SoftAssert();
        list1 = trelloAPI.createList(list1Name, board);
        softAssert.assertEquals(list1.getName(), list1Name, "Wrong 1st list name after creation list by API call");
        softAssert.assertEquals(list1.getIdBoard(), board.getId(), "1st list name was not created in board BOARD_API");
        list2 = trelloAPI.createList(list2Name, board);
        softAssert.assertEquals(list2.getName(), list2Name, "Wrong 2nd list name after creation list by API call");
        softAssert.assertEquals(list2.getIdBoard(), board.getId(), "2nd list name was not created in board BOARD_API");
        softAssert.assertAll();
    }

    @Stories("Create card in 1st list")
    @Test(dependsOnMethods = "testCreateList")
    public void testCreateCard() throws JSONException {
        SoftAssert softAssert = new SoftAssert();
        card = trelloAPI.createCard(cardName, list1); // create card in 1st list
        softAssert.assertEquals(card.getName(), cardName, "Wrong card name after creation card in 1st list");
        softAssert.assertEquals(card.getIdList(), list1.getId(), "Card is not included in 1st list");
        softAssert.assertAll();
    }

    @Stories("Create label to the 1st card")
    @Test(dependsOnMethods = "testCreateCard")
    public void testCreateLabel() throws JSONException {
        SoftAssert softAssert = new SoftAssert();
        Label label = trelloAPI.createLabel(labelName, card);
        softAssert.assertEquals(label.getName(), labelName, "Wrong card name after creation card in 1st list");
        softAssert.assertEquals(label.getIdBoard(), board.getId(), "Card is not included in 1st list");
        softAssert.assertAll();
    }

}