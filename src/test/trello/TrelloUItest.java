package trello;

import core.browsers.Browser;
import core.browsers.BrowserType;
import org.json.JSONException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageFactory.trello.BoardPage;
import pageFactory.trello.BoardsPage;
import pageFactory.trello.LoginPage;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.testng.Assert.assertEquals;

public class TrelloUItest {

    private Browser browser;
    private String url = "https://trello.com/login";
    private String boardName;
    private BoardPage boardPage;
    private String[] list = {"list1", "list2"};
    private String cardName = "cardName";

    @Parameters({"browser", "email", "password"})
    @BeforeClass
    public void initBrowser(BrowserType browser, String email, String password) throws JSONException {
        // init browser

        this.browser = new Browser(browser);
        this.browser.openLocaleBrowser();
        LoginPage loginPage = new LoginPage(this.browser);
        loginPage.openURL(url);
        loginPage.isOpened();
        loginPage.login(email, password);
    }

    @Stories("Create board")
    @Test
    public void testCreateBoard() {
        BoardsPage boardsPage = new BoardsPage(browser);
        boardPage = boardsPage
                .createNewBoard()
                .inputBoardName(boardName);
        assertEquals(boardPage.getBoardName(), boardName, "Wrong board name after board creation");
    }

    @Stories("Create list")
    @Test(dependsOnMethods = "testCreateBoard")
    public void testCreateList() {
        boardPage.addList(list);
        assertEquals(boardPage.getList(), list, "Wrong list after creation list1 and list2");
    }

    @Stories("Add card")
    @Test(dependsOnMethods = "testCreateList")
    public void testAddCard() {
        boardPage.addCard(cardName, 1); // add card to 1st list
        // check 1st card in 1st list
        assertEquals(boardPage.getCard(1, 1), cardName, "Wrong card in first list");
    }

    @Stories("Add label")
    @Test(dependsOnMethods = "testAddCard")
    public void testAddLabel() {
        boardPage.addCard(cardName, 1); // add card to 1st list
        // check 1st card in 1st list
        assertEquals(boardPage.getCard(1, 1), cardName, "Wrong card in first list");
    }

    @AfterTest
    public void close() {
        browser.close();
    }
}