package core.browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.util.HashMap;
import java.util.Map;

public class ChromeDriverMob extends ChromeDriverFactory {

    private ChromeOptions options;
    private DesiredCapabilities capabilities;

    public ChromeDriverMob(){
        super();
    }

    @Override
    public WebDriver getInstance(){
        options = new ChromeOptions();
        capabilities = DesiredCapabilities.chrome();

        Map<String, String> mobileEmulation = new HashMap<String, String>();
        mobileEmulation.put("deviceName", "Google Nexus 5");
        options.setExperimentalOption("mobileEmulation", mobileEmulation);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        return new ChromeDriver(capabilities);
    }
}
