package core.browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import utils.hosts.Host;
import utils.hosts.HostConfig;
import utils.locations.LocaleData;

import java.net.MalformedURLException;

public class FirefoxDriverFactory extends WebDriverFactory {

    private FirefoxProfile profile;
    private DesiredCapabilities capabilities;
    private Host host;

    FirefoxDriverFactory() {
        System.setProperty("webdriver.gecko.driver", this.findDriver() + "geckodriver.exe");
    }

    public FirefoxDriverFactory(Host host, String location, int waitInSeconds) {
        profile = new FirefoxProfile();
        capabilities = DesiredCapabilities.firefox();
        profile.setPreference("intl.accept_languages", new LocaleData().getLocation(location));

        int waitInSeconds1 = waitInSeconds;
        this.host = host;
    }

    @Override
    public WebDriver getInstance() {
        return new FirefoxDriver();
    }

    @Override
    public WebDriver openRemoteBrowser() throws MalformedURLException {
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        return new RemoteWebDriver(HostConfig.setHost(this.host), capabilities);
    }
}
