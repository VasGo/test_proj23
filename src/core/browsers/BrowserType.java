package core.browsers;

public enum BrowserType {
    IE11, CHROME, CHROME_MOB, FF, SAFARI;
}
