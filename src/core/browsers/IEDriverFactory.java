package core.browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.net.MalformedURLException;

public class IEDriverFactory extends WebDriverFactory {

    public IEDriverFactory() {
        System.setProperty("webdriver.ie.driver", this.findDriver() + "IEDriverServer.exe");
    }

    @Override
    public WebDriver getInstance() {
        return new InternetExplorerDriver();
    }

    @Override
    public WebDriver openRemoteBrowser() throws MalformedURLException {
        return null;
    }
}
