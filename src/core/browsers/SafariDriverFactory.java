package core.browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;

public class SafariDriverFactory extends WebDriverFactory {

    public SafariDriverFactory() {
        System.setProperty("webdriver.safari.driver", this.findDriver() + "IEDriverServer.exe");
    }

    @Override
    public WebDriver getInstance() {
        return new SafariDriver();
    }

    @Override
    public WebDriver openRemoteBrowser() throws MalformedURLException {
        return null;
    }
}
