package core.browsers;

import org.openqa.selenium.WebDriver;
import utils.hosts.Host;

import java.net.MalformedURLException;

public abstract class WebDriverFactory {

    private String osName = System.getProperty("os.name").toLowerCase();
    private String osArch = System.getProperty("os.arch").toLowerCase();

    public abstract WebDriver getInstance();

    public abstract WebDriver openRemoteBrowser() throws MalformedURLException;

    String findDriver() {
        String pathToDriver = "resources/drivers/";
        if (osName.contains("linux") || osName.contains("nix")) {
            return pathToDriver + "/linux/";
        } else if (osName.contains("mac") || osName.contains("osX")) {
            return pathToDriver + "/mac/";
        } else {
            return pathToDriver + "/windows/";
        }
    }

    public static WebDriver initBrowser(BrowserType browserType) {
        switch (browserType) {
            case CHROME:
                return new ChromeDriverFactory().getInstance();
            case FF:
                return new FirefoxDriverFactory().getInstance();
            case IE11:
                return new IEDriverFactory().getInstance();
            case SAFARI:
                return new SafariDriverFactory().getInstance();
            case CHROME_MOB:
                return new ChromeDriverMob().getInstance();
            default:
                return null;
        }
    }

    public static WebDriverFactory initRemoteBrowser(
            BrowserType browserType,
            Host host,
            String location,
            int waitInSeconds
    ) {
        switch (browserType) {
            case CHROME:
                return new ChromeDriverFactory(host, location, waitInSeconds);
            case FF:
                return new FirefoxDriverFactory(host, location, waitInSeconds);
            default:
                return null;
        }
    }
}
