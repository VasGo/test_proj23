package core.browsers;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import utils.hosts.Host;
import utils.locations.Locations;
import utils.tools.Log;

import java.net.MalformedURLException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Browser {

    private WebDriver driver;
    private BrowserType browser;
    private String location;
    private String handle;
    private String name;
    private String parentHandle;
    private int instanceCount;

    public Browser(BrowserType browser) {
        this.browser = browser;
    }

    public WebDriver getDriver() {
        return this.driver;
    }

    public void openLocaleBrowser() {
        driver = WebDriverFactory.initBrowser(browser);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        Log.info("Open browser: " + browser.name());
    }

    /**
     * @param width  - browser width
     * @param height - browser height
     */
    public void openLocaleBrowser(int width, int height) {
        driver = WebDriverFactory.initBrowser(browser);
        driver.manage().window().setSize(new Dimension(width, height));
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        Log.info("Open browser: " + browser.name());
    }

    /**
     * @param location - location(USA, GBR, etc.)
     */
    public void openLocaleBrowser(String location) {
        openLocaleBrowser();
        this.setLocation(Locations.valueOf(location));
    }

    public void openRemoteBrowser(Host host, String location, int waitInSeconds) throws MalformedURLException {
        driver = WebDriverFactory.initRemoteBrowser(browser, host, location, waitInSeconds).openRemoteBrowser();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        Log.info("Open remote browser: " + browser.name());
    }

    public void openChromeMob() {
        driver = WebDriverFactory.initBrowser(browser);
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        Log.info("Open chrome mobile");
    }

    public void setLocation(Locations location) {
        driver.manage().addCookie(new Cookie("ip_address", location.getIp()));
    }

    public void openNewTab() {
        parentHandle = driver.getWindowHandle();
        name = createUniqueName();
    }

    public void closeOtherTabs(WebDriver driver) {
        String currentHandle = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles();

        for (String handle : handles) {
            if (!handle.equals(currentHandle)) {
                driver.switchTo().window(handle);
                driver.close();
            }
        }
        driver.switchTo().window(currentHandle);
    }

    public void close() {
        this.driver.quit();
        Log.info("Close browser: " + browser.name());
    }

    private String createUniqueName() {
        return "a_Web_Window_" + ++instanceCount;
    }
}
