package core.browsers;

import utils.hosts.Host;
import utils.hosts.HostConfig;
import utils.locations.LocaleData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;

public class ChromeDriverFactory extends WebDriverFactory {

    private ChromeOptions options;
    private DesiredCapabilities capabilities;
    private Host host;

    public ChromeDriverFactory() {
        System.setProperty(
                "webdriver.chrome.driver",
                this.findDriver() + "chromedriver.exe");
    }

    public ChromeDriverFactory(Host hubHost, String location, int waitInSeconds){
        options = new ChromeOptions();
        options.addArguments("--lang=" + new LocaleData().getLocation(location));
        options.addArguments("--disable-user-media-security");
        options.addArguments("--use-fake-ui-for-media-stream");
        capabilities = DesiredCapabilities.chrome();
        this.host = host;
    }

    @Override
    public WebDriver getInstance() {
        return new ChromeDriver();
    }

    @Override
    public WebDriver openRemoteBrowser() throws MalformedURLException {
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        return new RemoteWebDriver(HostConfig.setHost(this.host), capabilities);
    }
}
