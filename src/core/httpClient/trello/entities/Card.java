package core.httpClient.trello.entities;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

public class Card {

    private String id;
    private String name;
    private String idList;
    private JSONObject response;

    public Card(JSONObject response) {
        this.response = response;
    }

    public String getId() {
        return id;
    }

    public void setId() {
        try {
            id = response.getString("id");
        } catch (JSONException e) {
            Assert.fail("Cannot get card id from JSON closableResponse: " + response);
        }
    }

    public String getName() {
        return name;
    }

    public void setName() {
        try {
            name = response.getString("name");
        } catch (JSONException e) {
            Assert.fail("Cannot get list name from JSON closableResponse: " + response);
        }
    }

    public String getIdList() {
        return idList;
    }

    public void setIdList() {
        try {
            idList = response.getString("idList");
        } catch (JSONException e) {
            Assert.fail("Cannot get idList from JSON closableResponse: " + response);
        }
    }
}