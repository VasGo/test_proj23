package core.httpClient.trello.entities;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

/**
 * Created by Aleksey123 on 11/4/2018.
 */
public class Board {

    private String id;
    private String name;
    private JSONObject response;

    public Board(JSONObject response) {
        this.response = response;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName() {
        try {
            name = response.getString("name");
        } catch (JSONException e) {
            Assert.fail("Cannot get board name from JSON closableResponse: " + response);
        }
    }

    public void setId() {
        try {
            id = response.getString("id");
        } catch (JSONException e) {
            Assert.fail("Cannot get board id from JSON closableResponse: " + response);
        }
    }
}
