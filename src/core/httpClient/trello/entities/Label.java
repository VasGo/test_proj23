package core.httpClient.trello.entities;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

public class Label {

    private String id;
    private String idBoard;
    private String name;
    private String color;
    private JSONObject response;

    public Label(JSONObject response) {
        this.response = response;
    }

    public String getId() {
        return id;
    }

    public void setId() {
        try {
            id = response.getString("id");
        } catch (JSONException e) {
            Assert.fail("Cannot get id label from JSON closableResponse: " + response);
        }
    }

    public String getName() {
        return name;
    }

    public void setName() {
        try {
            name = response.getString("name");
        } catch (JSONException e) {
            Assert.fail("Cannot get label name from JSON closableResponse: " + response);
        }
    }

    public String getIdBoard() {
        return idBoard;
    }

    public void setIdBoard() {
        try {
            idBoard = response.getString("idBoard");
        } catch (JSONException e) {
            Assert.fail("Cannot get idBoard from JSON closableResponse: " + response);
        }
    }
}