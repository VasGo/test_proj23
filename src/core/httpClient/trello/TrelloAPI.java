package core.httpClient.trello;

import com.google.common.net.HttpHeaders;
import core.httpClient.HttpClient;
import core.httpClient.trello.entities.Board;
import core.httpClient.trello.entities.BoardList;
import core.httpClient.trello.entities.Card;
import core.httpClient.trello.entities.Label;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class TrelloAPI extends HttpClient {

    private static final String URL = "https://api.trello.com";
    private static final String END_POINT_BOARD = "/1/boards";
    private static final String END_POINT_LIST = "/1/lists";
    private static final String END_POINT_CARD = "/1/cards";
    private String key = "3c4eaf37cd9bab94390ed06fb0523939";
    private String token = "97e2b43b314f4301814210d83b97e3ec97543e2a5c678614987f1ca17639f5e9";

    public Board createBoard(String name) throws JSONException {
        this.setHeader(HttpHeaders.ACCEPT, "application/json");
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("key", key));
        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("defaultLabels", "true"));
        params.add(new BasicNameValuePair("defaultLists", "true"));
        params.add(new BasicNameValuePair("keepFromSource", "none"));
        params.add(new BasicNameValuePair("prefs_permissionLevel", "private"));
        params.add(new BasicNameValuePair("prefs_voting", "disabled"));
        params.add(new BasicNameValuePair("prefs_comments", "members"));
        params.add(new BasicNameValuePair("prefs_invitations", "members"));
        params.add(new BasicNameValuePair("prefs_selfJoin", "true"));
        params.add(new BasicNameValuePair("prefs_cardCovers", "true"));
        params.add(new BasicNameValuePair("prefs_background", "blue"));
        params.add(new BasicNameValuePair("prefs_cardAging", "regular"));

        post(URL + END_POINT_BOARD, params);
        execute();
        Board board = new Board(getJSONresponse());
        board.setId();
        board.setName();

        return board;
    }

    public BoardList createList(String name, Board board) throws JSONException {
        this.setHeader(HttpHeaders.ACCEPT, "application/json");
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("key", key));
        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("idBoard", board.getId()));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("pos", "bottom"));

        post(URL + END_POINT_LIST, params);
        execute();
        BoardList list = new BoardList(getJSONresponse());
        list.setId();
        list.setIdBoard();
        list.setName();

        return list;
    }

    public Card createCard(String name, BoardList list) throws JSONException {
        this.setHeader(HttpHeaders.ACCEPT, "application/json");
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("key", key));
        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("idList", list.getId()));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("keepFromSource", "all"));

        post(URL + END_POINT_CARD, params);
        execute();
        Card card = new Card(getJSONresponse());
        card.setId();
        card.setIdList();
        card.setName();

        return card;
    }

    public Label createLabel(String name, Card card) throws JSONException {
        this.setHeader(HttpHeaders.ACCEPT, "application/json");
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("key", key));
        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("id", card.getId()));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("color", "null"));

        post(URL + "/1/cards/" + card.getId() + "/labels", params);
        execute();
        Label label = new Label(getJSONresponse());
        label.setId();
        label.setIdBoard();
        label.setName();

        return label;
    }
}