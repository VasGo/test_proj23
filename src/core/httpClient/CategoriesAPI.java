package core.httpClient;

import com.google.common.net.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import utils.tools.Log;

import java.util.ArrayList;
import java.util.List;

public class CategoriesAPI extends HttpClient {

    private String keyAPI;

    private List<String> filesWithFormat;
    private List<String> filesWithOutFormat;

    public CategoriesAPI(String keyAPI) {
        this.keyAPI = "tkn_name " + keyAPI;
    }

    @Override
    public HttpClient get(String url) {
        super.get(url);
        this.setHeader(HttpHeaders.ACCEPT, "application/json");
        this.setHeader(HttpHeaders.AUTHORIZATION, keyAPI);

        return this;
    }

    @Override
    public HttpClient post(String url, List<NameValuePair> post) {
        super.post(url, post);
        this.setHeader(HttpHeaders.ACCEPT, "application/json");
        this.setHeader(HttpHeaders.AUTHORIZATION, keyAPI);

        return this;
    }

    public List<NameValuePair> uploadFile(String filePath) {
        List<NameValuePair> file = new ArrayList<>();
        file.add(new BasicNameValuePair("file", filePath));

        return file;
    }

    public List<String> getCategoriesWithFormats(JSONObject json) {
        filesWithFormat = new ArrayList<>();

        try {
            for (int i = 0; i < json.getJSONArray("items").length(); i++) {
                filesWithFormat.add((String) json.getJSONArray("items").getJSONObject(i).get("name"));
            }
        } catch (JSONException e) {
            Log.error("Cannot get category with format from JSON: " + json.toString());
            Assert.fail("Cannot get category with format from JSON: " + json.toString());
        }

        return filesWithFormat;
    }

    public List<String> getCategoriesWithOutFormats(JSONObject json) throws JSONException {
        String buffer;
        filesWithOutFormat = new ArrayList<>();

        try {
            for (int i = 0; i < json.getJSONArray("items").length(); i++) {
                buffer = json.getJSONArray("items").getJSONObject(i).getString("name");
                filesWithOutFormat.add(
                        buffer.replace("." + json.getJSONArray("items").getJSONObject(i).getString("type"),
                                "")
                );
            }
        } catch (JSONException e) {
            Log.error("Cannot get category without format from JSON: " + json.toString());
            Assert.fail("Cannot get category without format from JSON: " + json.toString());
        }

        return filesWithOutFormat;
    }

}
