package core.httpClient;

import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import utils.tools.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public class HttpClient {

    BasicCookieStore cookieStore;
    private HttpGet getRequest;
    private HttpPut httpPut;
    private HttpEntity entity;
    private HttpPost postRequest;
    protected CloseableHttpClient client;
    public boolean mobileUserAgent = false;
    protected CloseableHttpResponse closableResponse;
    protected HttpResponse response;
    protected String content;

    public HttpClient() {
        cookieStore = new BasicCookieStore();
    }

    public int getStatusHttpResponse() {
        return closableResponse.getStatusLine().getStatusCode();
    }

    /**
     * @param url String, where need to send GET
     * @return httpClient
     */
    public HttpClient get(String url) {
        getRequest = new HttpGet(url);
        return this;
    }

    public HttpClient put(String url) {
        httpPut = new HttpPut(url);

        return this;
    }


    /**
     * @return current executed content
     */
    public String getContent() {
        return this.content;
    }

    public JSONObject getJSONresponse() throws JSONException {
        return new JSONObject(this.content);
    }

    public HttpEntity getEntity() {
        return entity;
    }

    /**
     * @param url  String, where need to send POST
     * @param post BoardList of Post data OR BoardList<NameValuePair>
     * @return httpClient
     */
    public HttpClient post(String url, List<NameValuePair> post) {
        postRequest = new HttpPost(url);

        try {
            postRequest.setEntity(new UrlEncodedFormEntity(post));
            Log.info(postRequest.getEntity().toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return this;
    }

    public HttpClient post(String url, JSONObject json) {
        postRequest = new HttpPost(url);
        try {
            StringEntity entity = new StringEntity(json.toString());
            postRequest.setEntity(entity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return this;
    }

    public HttpClient post(String url, String stringData) {
        postRequest = new HttpPost(url);
        try {
            StringEntity entity = new StringEntity(stringData);
            postRequest.setEntity(entity);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return this;
    }

    /**
     * This method is need to execute query;
     *
     * @return httpClient
     */
    public HttpClient execute() {
        HttpClientContext context = HttpClientContext.create();
        try {
            String mobSite = "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.23 Mobile Safari/537.3";
            if (mobileUserAgent) {
                client = HttpClients.custom()
                        .setRedirectStrategy(new LaxRedirectStrategy())
                        .setDefaultCookieStore(cookieStore)
                        .setUserAgent(mobSite)
                        .build();
            } else {
                client = HttpClients.custom()
                        .setRedirectStrategy(new LaxRedirectStrategy())
                        .setDefaultCookieStore(cookieStore)
                        .build();
            }
            if (getRequest != null) {
                if (mobileUserAgent) {
                    getRequest.setHeader(HttpHeaders.USER_AGENT, mobSite);
                }
                closableResponse = client.execute(getRequest);
            }
            if (postRequest != null) {
                if (mobileUserAgent) {
                    postRequest.setHeader(HttpHeaders.USER_AGENT, mobSite);
                }
                closableResponse = client.execute(postRequest);
            }
            if (postRequest != null) {
                HttpHost target = context.getTargetHost();
                List<URI> redirectLocations = context.getRedirectLocations();
                URI location = URIUtils.resolve(postRequest.getURI(), target, redirectLocations);

                entity = closableResponse.getEntity();
                content = EntityUtils.toString(entity);
                EntityUtils.consume(entity);
                postRequest = null;
                return this;

            }
            if (getRequest != null) {
                HttpHost target = context.getTargetHost();
                List<URI> redirectLocations = context.getRedirectLocations();
                URI location = URIUtils.resolve(getRequest.getURI(), target, redirectLocations);
                String locationUrl = location.toASCIIString();

                entity = closableResponse.getEntity();
                content = EntityUtils.toString(entity);
                EntityUtils.consume(entity);
                postRequest = null;
                return this;
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        return this;
    }

    /**
     * @param key   name of header
     * @param value value of header
     * @return httpClient
     */
    public HttpClient setHeader(String key, String value) {
        if (getRequest != null) {
            getRequest.addHeader(key, value);
        }
        if (postRequest != null) {
            postRequest.addHeader(key, value);
        }

        return this;
    }

    public HttpClient setXHRHeader() {
        this.setHeader(com.google.common.net.HttpHeaders.X_REQUESTED_WITH, "XMLHttpRequest");
        return this;
    }

    /**
     * @param url   String, where need to set cookie
     * @param key   name of cookie
     * @param value value of cookie
     * @return httpClient
     */
    public HttpClient setCookie(String url, String key, String value) {
        BasicClientCookie cookie = new BasicClientCookie(key, value);
        cookieStore.addCookie(cookie);
        String domain = url.split("//")[1];
        cookie.setDomain(domain);
        cookie.setPath("/");
        cookieStore.addCookie(cookie);

        return this;
    }

}
